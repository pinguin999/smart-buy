from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('details/<int:id>/', views.details, name='details'),
    path('order/<int:id>', views.order, name='order'),
    path('myorders', views.myorders, name='myorders'),
    path('setup', views.setup, name='setup'),
    path('signup', views.SignUpView.as_view(), name='signup'),
    path('accounts/', include('django.contrib.auth.urls')),

    # ###################### BACKEND #############################
    path('backend', views.backend_index, name='backend_index'),

]

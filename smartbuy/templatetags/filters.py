from django import template
from smartbuy.views import html_to_dict
import json

register = template.Library()


@register.filter()
def article_get_titel(article):
    return html_to_dict(article)['titel']


@register.filter()
def article_get_image(article):
    return html_to_dict(article)['image']


@register.filter()
def pass_get_url(your_pass):
    return json.loads(your_pass)['url']

import requests
import json
import os
from datetime import datetime, timedelta
from django.conf import settings

ENV = 'dev'  # dev or prod

API_URL = 'https://api.yourpass.eu' if ENV == 'prod' else 'https://pass.ysplay.cz/api'
DISTRIBUTION_URL = 'https://get.yourpass.eu' if ENV == 'prod' else 'https://pass.ysplay.cz/pass'
ISSUE_URL = 'https://issue.yourpass.eu' if ENV == 'prod' else 'https://pass.ysplay.cz/issue'
CONSOLE_URL = 'https://console.yourpass.eu' if ENV == 'prod' else 'https://pass-console.ysplay.cz'

ACCESS_TOKEN = None
ACCESS_TOKEN_TIME = None

if settings.USER is None or settings.PASS is None:
    if 'K8S_SECRET_YOURPASSUSER' in os.environ:
        settings.USER = os.environ['K8S_SECRET_YOURPASSUSER']
        settings.PASS = os.environ['K8S_SECRET_YOURPASSPASS']
    else:
        settings.USER = os.environ['YOURPASSUSER']
        settings.PASS = os.environ['YOURPASSPASS']


def _get_access_token(mail, password):
    #  curl --request POST   --url https://api.yourpass.eu/oauth2/token   --header 'Authorization: Basic YzM2YjY3MjEtMDRkNS00ZGNlLWIxZjItNDc5NmQ4ZmNjODQ5Og=='
    #  --header 'Cache-Control: no-cache'   --header 'Content-Type: application/x-www-form-urlencoded' --data 'grant_type=password&username=x@y.com&password=XX'

    headers = {'Authorization': 'Basic YzM2YjY3MjEtMDRkNS00ZGNlLWIxZjItNDc5NmQ4ZmNjODQ5Og==',
               'Cache-Control': 'no-cache',
               'Content-Type': 'application/x-www-form-urlencoded'}
    data = f'grant_type=password&username={mail}&password={password}'
    response = requests.post(url='https://api.yourpass.eu/oauth2/token', headers=headers, data=data)
    return json.loads(response.text)['access_token'], datetime.now() + timedelta(seconds=json.loads(response.text)['expires_in'])


def set_access_token_global():
    global ACCESS_TOKEN
    global ACCESS_TOKEN_TIME
    ACCESS_TOKEN, ACCESS_TOKEN_TIME = _get_access_token(settings.USER, settings.PASS)


def get_global_access_token():
    global ACCESS_TOKEN
    global ACCESS_TOKEN_TIME
    if not ACCESS_TOKEN_TIME or ACCESS_TOKEN_TIME < datetime.now():
        set_access_token_global()
    return ACCESS_TOKEN


def create_pass(dynamic_data):
    # curl -X POST 'https://pass.ysplay.cz/api/v1/pass' \
    #      -H 'authorization: Bearer eyJhbGciOiJFUzI1NiIsInR5cCI6IkpXVCJ9.xyz' \
    #      -H 'content-type: application/json' \
    #      -d '{ "dynamicData": { "qrcode": "12345678", "fullName": "Mr. Tomáš Dvě", "cardNumber": "12345678" },
    #   "templateId": "d1d26dbb-41c5-4c26-9c56-227bf4692aae" }'
    headers = {'authorization': f'Bearer {get_global_access_token()}', 'content-type': 'application/json'}
    data = {"dynamicData": dynamic_data, "templateId": "d1d26dbb-41c5-4c26-9c56-227bf4692aae"}
    response = requests.post(url=f'{API_URL}/v1/pass', headers=headers, data=json.dumps(data))
    if response.status_code != 201:
        raise Exception(f'Error creating Pass {response.text}')
    return response


def get_template(template='d1d26dbb-41c5-4c26-9c56-227bf4692aae'):
    # curl -X GET '<API_URL>/v1/template/:id' \
    #  -H 'authorization: Bearer <JWT_TOKEN>'
    headers = {'authorization': f'Bearer {get_global_access_token()}'}
    response = requests.get(url=f'{API_URL}/v1/template/{template}', headers=headers)
    return response


def update_pass(pass_id, dynamic_data):
    # curl -X PUT '<API_URL>/v1/pass/:id' \
    #  -H 'authorization: Bearer <JWT_TOKEN>' \
    #  -H 'content-type: application/json' \
    #  -d '{"dynamicData":{"qrcode":"12345678","fullName":"Mr. Tomáš Dvě","cardNumber":"12345678"},"templateId":"id-of-template"}'
    headers = {'authorization': f'Bearer {get_global_access_token()}', 'content-type': 'application/json'}
    data = {"dynamicData": dynamic_data, "templateId": "d1d26dbb-41c5-4c26-9c56-227bf4692aae"}
    response = requests.put(url=f'{API_URL}/v1/pass/{pass_id}', headers=headers, data=json.dumps(data))
    if response.status_code != 200:
        raise Exception(f'Error updating Pass {response.text}')
    return response

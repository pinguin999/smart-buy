from django.contrib import admin
from import_export import resources
from import_export.admin import ImportExportModelAdmin

# Register your models here.
from .models import Article


class ArticleResource(resources.ModelResource):

    class Meta:
        model = Article


@admin.register(Article)
class PersonAdmin(ImportExportModelAdmin):
    pass

# admin.site.register(Article)

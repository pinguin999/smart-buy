from smartbuy.models import Article
from django import forms
import datetime
from django.core.exceptions import ValidationError
from django.core.validators import DecimalValidator


class PriceField(forms.CharField):
    def to_python(self, value):
        value = value.strip()
        if value.endswith('€'):
            value = value[:-1].strip()
        if ',' in value and '.' not in value:
            value = value.replace(',', '.')
        return value

    def validate(self, value):
        super().validate(value)
        try:
            float(value)
        except ValueError:
            raise ValidationError(
                'Bitte geben Sie einen gültigen Preis ein.',
                params={'value': value},
            )
        if float(value) < 0.0:
            raise ValidationError(
                'Bitte geben Sie einen positiven Preis ein.',
                params={'value': value},
            )


class BuyForm(forms.Form):
    price = PriceField(widget=forms.TextInput(
        attrs={'rows': 1, 'placeholder': "   gib deinen Wunschpreis ...", 'style': "font-size: 2em;"}))
    amount = forms.DecimalField(validators=[DecimalValidator(decimal_places=0, max_digits=4)], label='', min_value=1, widget=forms.TextInput(
        attrs={'rows': 1, 'placeholder': "... und die Menge ein", 'style': "font-size: 2em;"}))
    until = forms.DateField(label='Spätestes Lieferdatum', initial=datetime.date.today() + datetime.timedelta(days=(365 / 2)))
    article = forms.ModelChoiceField(queryset=Article.objects.all(), widget=forms.HiddenInput())

    def clean_until(self):
        until = self.cleaned_data['until']
        if until <= datetime.date.today():
            raise forms.ValidationError("Das späteste Lieferdatum muss in der Zukunft sein.")
        return until

    # def clean_price(self):
    #     from smartbuy.views import html_to_dict
    #     price = self.cleaned_data['price']
    #     article_price = html_to_dict(self.cleaned_data['article'])['price']
    #     if price > article_price:
    #         raise forms.ValidationError("Der Wunschpreis ist größer als der aktuelle Preis.")
    #     return price


class SearchForm(forms.Form):
    search = forms.CharField(label='', widget=forms.TextInput(
        attrs={'rows': 1, 'placeholder': "Suche ...", 'style': "font-size: 2em;"}))

from django.db import models
from django.contrib.auth.models import User
from django.db.models.deletion import CASCADE

# Create your models here.


class Article(models.Model):
    article_html = models.TextField()
    name = models.TextField()
    date_updated = models.DateTimeField('date updated')
    url = models.URLField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    working = models.BooleanField(default=True)

    def __str__(self):
        return self.article_html[:200]


class Order(models.Model):
    article = models.ForeignKey(Article, on_delete=CASCADE)
    user = models.ForeignKey(User, on_delete=CASCADE)
    amount = models.FloatField()
    price = models.FloatField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    delivered = models.DateTimeField(default=None, null=True)
    your_pass_json = models.TextField()

from django.shortcuts import redirect, render
import requests
from bs4 import BeautifulSoup
from .models import Article, Order
from datetime import datetime
from django.urls import reverse, reverse_lazy
from django.shortcuts import get_object_or_404
from django.views import generic
from django.contrib.auth.forms import UserCreationForm
import json
from .forms import BuyForm, SearchForm
from django.contrib.auth.decorators import login_required
from .your_pass import create_pass
from functools import lru_cache
from typing import Optional
from django.http import HttpResponseRedirect


@lru_cache(maxsize=500)
def get_product(URL) -> Optional[Article]:
    page = requests.get(URL)

    # article = Article.objects.create(article_html=page.content.decode('utf-8'), date_updated=datetime.now(), url=URL)

    article, created = Article.objects.get_or_create(
        article_html=page.content.decode('utf-8', errors="ignore"),
        defaults={'article_html': page.content.decode('utf-8', errors="ignore"), 'date_updated':  datetime.now(), 'url': URL},
    )
    try:
        article.name = html_to_dict(article)['titel']
        article.save()
        return article
    except TypeError:
        article.working = False
        article.save()
        return None


def setup(request):

    products = ['https://www.idealo.de/preisvergleich/OffersOfProduct/200312717_-paper-mario-the-origami-king-switch.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/200743509_-iphone-12-pro-128gb-graphit-apple.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/200479070_-io-series-9n-oral-b.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/200585559_-call-of-duty-black-ops-cold-war-ps5.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/6674372_-series-9-9385cc-graphite-braun.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/6046043_-alpha-7-iii-body-sony.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/200176180_-oled-cx9la-lg-electronics.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/6497976_-ultima-40-surround-5-1-set-mk3-teufel.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/200281369_-macbook-pro-13-2020-mxk32d-a-apple.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/200637333_-assassin-s-creed-valhalla-ps5.html',
                'https://www.idealo.de/preisvergleich/OffersOfProduct/6639723_-ryzen-9-3900x-amd.html',
                'https://geizhals.de/amd-ryzen-9-5900x-100-100000061wof-a2392526.html',
                ]

    for URL in products:
        get_product(URL)

    return redirect(reverse('index'))


@lru_cache(maxsize=500)
def html_to_dict(article: Article):
    article_out = {}

    soup = BeautifulSoup(article.article_html, 'html.parser')

    article_out['url'] = article.article_html

    article_out['id'] = article.id
    try:
        if article.url.startswith('https://geizhals.de'):
            article_out['titel'] = soup.find_all(class_='variant__header__headline')[0].text

            price = soup.find_all(class_='gh_price')[0].text.replace('€ ', '')
            if ',' in price and '.' not in price:
                price = price.replace(',', '.')
            article_out['price'] = float(price)

            article_out['image'] = (soup.find_all(class_='gal_img')[0]['src'])
        else:
            article_out['titel'] = soup.find_all(class_='oopStage-title')[0].span.text
            article_out['price'] = json.loads(soup.find_all(class_='pageContent-wrapper')[0].script.next)['offers']['lowPrice']
            article_out['image'] = (soup.find_all(class_='rsImg rsImg--first')[0]['href'])
    except IndexError:
        article.working = False
        article.save()
        return
    return article_out


def index(request):

    articles = []
    articles_ = []

    if request.method == 'POST':
        form = SearchForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            page = requests.get(f'https://geizhals.de/?fs={form.cleaned_data["search"].replace(" ", "+")}')

            if 'Sorry - 429 Too Fast' in page.content.decode('utf-8'):
                return redirect(reverse('index'))

            soup = BeautifulSoup(page.content.decode('utf-8'), 'html.parser')

            for i in range(0, 10):
                if soup.find_all(class_='listview__content')[i].text.find("Varianten") == -1:
                    URL = 'https://geizhals.de/' + soup.find_all(class_='listview__name-link')[i]['href']
                    if get_product(URL) is not None:
                        articles_.append(get_product(URL))
                    if len(articles_) >= 3:
                        break

    else:
        articles_ = Article.objects.filter(working=True).order_by('-created_at')[:12]
        if len(articles_) == 0:
            return redirect(reverse('setup'))

        form = SearchForm()

    for article in articles_:
        article_out = html_to_dict(article)
        if article_out:
            articles.append(article_out)

    return render(request, 'index.html', {
        'Titel': 'Smart Buy',
        'articles': articles,
        'form': form,
    })


@login_required
def details(request, id):
    article = get_object_or_404(Article, pk=id)
    form = BuyForm(initial={'article': article})
    if request.method == 'POST':
        form = BuyForm(request.POST)
        # check whether it's valid:
        if form.is_valid():
            price = float(form.cleaned_data['price'])
            # kaufen
            order = Order.objects.create(
                user=request.user,
                article=form.cleaned_data['article'],
                amount=form.cleaned_data['amount'],
                price=price
            )

            article = html_to_dict(form.cleaned_data['article'])

            dynamic_data = {
                'amount': int(order.amount),
                'desired_price': order.price,
                'price': f"{article['price']} €",
                'product': article['titel'][0:20],
            }

            your_pass = create_pass(dynamic_data).text
            order.your_pass_json = your_pass
            order.save()

            return HttpResponseRedirect('/order/{}'.format(order.id))

    article_out = html_to_dict(article)

    return render(request, 'details.html', {
        'Titel': 'Smart Buy',
        'article': article_out,
        'form': form,
    })


@login_required
def order(request, id):
    order = get_object_or_404(Order, user=request.user, pk=id)
    your_pass = json.loads(order.your_pass_json)

    return render(request, 'order.html', {
        'Titel': 'Smart Buy',
        'article': html_to_dict(order.article),
        'price': order.price,
        'amount': order.amount,
        'your_pass_url': your_pass['url'],
    })


class SignUpView(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'registration/signup.html'


@login_required
def myorders(request):

    orders = Order.objects.filter(user=request.user)

    return render(request, 'myorders.html', {
        'Titel': 'Smart Buy',
        'orders': orders,
    })


# ############################# BACKEND ####################################


def backend_index(request):

    articles = []
    orders = Order.objects.all()
    articles_ = Article.objects.filter(working=True)

    for article in articles_:
        article_out = html_to_dict(article)
        if article_out:
            articles.append(article_out)

    return render(request, 'backend/index.html', {
        'Titel': 'Smart Buy',
        'orders': orders,
        'articles': articles,
    })
